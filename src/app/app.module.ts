import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoEditComponent } from './todo-list/todo-edit/todo-edit.component';
import { todoDataService } from './todo-list/todo-data.service';
import { listService } from './todo-list/todo-list.service';
import { AuthenticationComponent } from './authentication/authentication.component';
import { RoutingModule } from './routing.module';
import { authenticationService } from './authentication/authentication.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TodoListComponent,
    TodoEditComponent,
    AuthenticationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RoutingModule
  ],
  providers: [todoDataService, listService, authenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
