import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { authenticationService, responsePayload } from './authentication.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent {

  signingUp = true;
  errorMsg : string;

  constructor(private authenticationService: authenticationService, private route: Router) {
    
  }

  authChange() {
    this.signingUp = !this.signingUp;
  }

  onSubmit(form: NgForm) {
    const useremail = form.value.email;
    const userpassword = form.value.password;
    this.errorMsg = null;

    let authObservable: Observable<responsePayload>;

    if (this.signingUp) {
      authObservable = this.authenticationService.signUp(useremail, userpassword)
    } else if (!this.signingUp) {
      authObservable = this.authenticationService.signIn(useremail, userpassword)
    }

    authObservable.subscribe(response => {
      console.log(response);
      this.route.navigate(['./todo-list']);
    }, errorMsg => {
      console.log(errorMsg);
      this.errorMsg = errorMsg;
    });
    
      form.reset();
  }

  ngOnInit(): void {
  }

}