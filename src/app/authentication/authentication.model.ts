export  class userData {
    constructor(private _token: string, private _tokenExpDate: Date, public email: string, public id: string, ) {
    }

    get token() {
        if (!this._tokenExpDate || new Date() > this._tokenExpDate) {
            return null;
        } return this._token;
    }
}