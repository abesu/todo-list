import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { throwError, Subject, BehaviorSubject } from 'rxjs';
import { userData } from './authentication.model';
import { Router } from '@angular/router';
import { listService } from '../todo-list/todo-list.service';

export interface responsePayload {
    kind: string;
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registered?: boolean;
}

@Injectable()

export class authenticationService { 
    tokenExists: boolean;

    constructor(private http: HttpClient, private route: Router, private lService: listService) {
    }

    private errorMsgs(error: HttpErrorResponse) {
        let errorMsg = 'Unknown error.'
            if (!error.error || !error.error.error) {
                errorMsg = 'Unknown error.'
                return throwError(errorMsg);
              } else {
              switch (error.error.error.message) {
                case 'EMAIL_EXISTS' :
                  errorMsg = 'The email address is already in use by another account';
                  break;
                case 'OPERATION_NOT_ALLOWED' : 
                  errorMsg = 'Password sign-in is disabled for this project';
                  break;
                case 'TOO_MANY_ATTEMPTS_TRY_LATER' : 
                  errorMsg = 'We have blocked all requests from this device due to unusual activity. Try again later.';
                  break;
                case 'EMAIL_NOT_FOUND' : 
                  errorMsg = 'There is no user record corresponding to this identifier. The user may have been deleted.';
                  break;
                case 'INVALID_PASSWORD' : 
                  errorMsg = 'The password is invalid or the user does not have a password.';
                  break;
                case 'USER_DISABLED' : 
                  errorMsg = 'The user account has been disabled by an administrator.';
                  break;  
                default : 
                  errorMsg = 'Unknown error.';
              } 
              return throwError(errorMsg);
            }
    };
 
    signUp(useremail: string, userpassword: string) {
        return this.http.post<responsePayload>('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCoo9q_6U1CoD5o4DsqRAz1CTrBRXWwves', {
            email: useremail,
            password: userpassword,
            returnSecureToken: true 
        }).pipe(catchError(this.errorMsgs), tap(response => {
          const tExpDate = new Date(new Date().getTime() + + response.expiresIn * 1000);
          const user = new userData(response.idToken, tExpDate, response.email, response.localId);
          this.user.next(user);
          localStorage.setItem('localUserData', response.idToken);
        }));
    }

    signIn(useremail: string, userpassword: string) {
        return this.http.post<responsePayload>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCoo9q_6U1CoD5o4DsqRAz1CTrBRXWwves', {
            email: useremail,
            password: userpassword,
            returnSecureToken: true 
        }).pipe(catchError(this.errorMsgs), tap(response => {
          const tExpDate = new Date(new Date().getTime() + + response.expiresIn * 1000);
          const user = new userData(response.idToken, tExpDate, response.email, response.localId);
          this.user.next(user);
          localStorage.setItem('localUserData', response.idToken);
        }));
    }

    signOut() {
      this.user.next(null);
      localStorage.removeItem("localUserData");
      this.lService.purgeList;
      this.route.navigate(['/authenticate']);
    }

    user = new Subject<userData>();

}
