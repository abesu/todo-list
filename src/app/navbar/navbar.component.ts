import { Component, OnInit, OnDestroy } from '@angular/core';
import { todoDataService } from '../todo-list/todo-data.service';
import { authenticationService } from '../authentication/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  private authSubscription: Subscription;
  userExists = false;
  userToken: string = null;

  constructor(private todoDataService: todoDataService, private authenticationService:authenticationService) {

  };

  ngOnInit() {
    
    this.authSubscription = this.authenticationService.user.subscribe(user => {
      this.userExists = !user ? false : true;
    } );
  }

  ngOnDestroy() {
  }

  onSignOut(){
    this.authenticationService.signOut();
  }

  /*uploadData() {
    this.todoDataService.uploadData();
  };

  downloadData() {
    this.todoDataService.downloadData();
    
  };*/
}
