import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';
import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';


const pagesRoutes: Routes = [
    { path: 'authenticate', component: AuthenticationComponent },
    { path: 'todo-list', component: TodoListComponent },
    //{ path: '', component: TodoListComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(pagesRoutes)],
    exports: [RouterModule]

})

export class RoutingModule{

}
