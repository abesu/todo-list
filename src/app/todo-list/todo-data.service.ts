import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { listService } from './todo-list.service';
import { ListItem } from './list.model';
import { authenticationService } from '../authentication/authentication.service';
import { Subscription } from 'rxjs';

@Injectable()
export class todoDataService {
    private authSubscription: Subscription;
    userToken: string;

    constructor(private http: HttpClient, private listService: listService, private authService: authenticationService) {
    }

    private uploadingData() {
        const items = this.listService.getLines();
        this.http.put('https://todo-list-33f59.firebaseio.com/list.json?auth=' + this.userToken, items).subscribe(Response => {
            console.log(Response);
        });
    }

    private downloadingData() {
        this.http.get<ListItem[]>('https://todo-list-33f59.firebaseio.com/list.json?auth=' + this.userToken).subscribe(lines => {
            this.listService.overWrite(lines);
            console.log(lines);
        })
    }

    uploadData() {
        this.userToken = localStorage.getItem("localUserData");
        /*this.authSubscription = this.authService.user.subscribe(user => {
            this.userToken = user.token;
          } );*/
        this.uploadingData();
    }

    downloadData() {
        this.userToken = localStorage.getItem("localUserData");
        //this.userToken = userToken;
        /*this.authSubscription = this.authService.user.subscribe(user => {
            this.userToken = user.token;
          } );*/
        this.downloadingData();
    }
}