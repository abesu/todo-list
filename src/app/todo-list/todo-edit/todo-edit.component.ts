import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ListItem } from '../list.model';
import { listService } from '../todo-list.service';
import { todoDataService } from '../todo-data.service';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit, OnDestroy {
  @ViewChild('formInp') inpForm: NgForm;
  subscribed: Subscription;
  editing = false;
  itemIndex: number;
  editItem: ListItem;

  //allowAdd = false;
 
  constructor(private lService: listService, private todoDataService: todoDataService) { }

  ngOnInit() {
    this.subscribed = this.lService.editingItem
    .subscribe(
      (index: number) => {
        this.itemIndex = index;
        this.editing = true;
        this.editItem = this.lService.getItemByIndex(index);
        (this.inpForm.controls['field']).setValue(this.editItem.line)
      }
    );
  };

  onNew(form: NgForm) {
    const value = form.value;
    const newEntry = new ListItem(value.field);
    if (this.editing) { 
      this.lService.editItem(this.itemIndex, newEntry);
      this.onReset();
    } else { 
      this.lService.newLine(newEntry);
  };
  this.todoDataService.uploadData();
};

  onInputChange(event: Event) {
    /*if ((<HTMLInputElement>event.target).value != '') {
      this.allowAdd = true;
    } else {
      this.allowAdd = false;
    }*/

  };

  ngOnDestroy() {
    this.subscribed.unsubscribe();
  }

  onReset() {
    this.inpForm.reset();
    this.editing = false;
  }

  onDelete(){
    this.lService.deleteItem(this.itemIndex);
    this.onReset();
    this.todoDataService.uploadData();
  }

}
