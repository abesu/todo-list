import { Component, OnInit, OnDestroy } from '@angular/core';
import { ListItem } from './list.model';
import { listService } from './todo-list.service';
import { Subscription } from 'rxjs';
import { todoDataService } from './todo-data.service';
 
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
  providers: [],
  styles: [`
    .checked {
      text-decoration: line-through;
    }
    `]
})

export class TodoListComponent implements OnInit, OnDestroy {
  lines: ListItem[]; 
  private listEditSubs: Subscription;
  

  constructor(private lService: listService, private todoDataService: todoDataService) { }

  ngOnInit() {
    this.todoDataService.downloadData();
    this.lines = this.lService.getLines();
    this.listEditSubs = this.lService.lineChanged.subscribe(
      (lines: ListItem[]) => {
        this.lines = lines;
      }
    )
  }

  ngOnDestroy (): void {
    this.listEditSubs.unsubscribe(); 
  }

  editItem(index: number) {
    this.lService.editingItem.next(index);
  }

}
