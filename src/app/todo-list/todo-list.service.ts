import { ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ListItem } from './list.model';
import { Subject } from 'rxjs';

export class listService {
    @ViewChild('formInp') inpForm: NgForm;
    lineChanged = new Subject<ListItem[]>();
    editingItem = new Subject<number>();
    editing = true;
    itemIndex: number;


    private lines: ListItem[] = [
        //new ListItem('test text to store in db')
      ];

      getLines() {
          return this.lines.slice();
      }

      newLine(line: ListItem){
          this.lines.push(line);
          this.lineChanged.next(this.lines.slice());
      }

      getItemByIndex(index: number) {
          return this.lines[index];
      }

      editItem(index: number, editedTask: ListItem) {
          this.lines[index] = editedTask;
          this.lineChanged.next(this.lines.slice());
      }

      deleteItem(index: number) {
        this.lines.splice(index, 1);
        this.lineChanged.next(this.lines.slice());
    }
    
      overWrite(lines: ListItem[]) {
        this.lines = lines;
        this.lineChanged.next(this.lines.slice());
    }

      purgeList(){
      }

      onReset() {
        this.inpForm.reset();
        this.editing = false;
      }
    
      onDelete(){
        this.deleteItem(this.itemIndex);
        this.onReset();
      }
}